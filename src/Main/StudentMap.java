package Main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.Comparator;

class RankCmp implements Comparator<String> {
	private HashMap<String, Student> map = null;
	public RankCmp(HashMap<String, Student> map) {
		this.map = map;
	}
	
	/**
	compare two students' score   
	@ parameter: idA, idB, two students' student ID
	@ return 1 if student A's score is better, -1 otherwise
	@ example: RankCmp.compare('985002032','985002033'), 
		then we get a merge of the labgrades and examgrades.
	@ Time estimate: O(1)
	*/
	
	@Override
	public int compare(String idA, String idB) {
		Student sa = map.get(idA);
		Student sb = map.get(idB);
		double avgScoreA = sa.getWeightedAverageScore();
		double avgScoreB = sb.getWeightedAverageScore();
		if (avgScoreA == avgScoreB) {
			return sa.getId().compareTo(sb.getId());
		}
		return avgScoreA < avgScoreB ? 1 : -1;
	}
}

public class StudentMap {
	private HashMap<String, Student> map = new HashMap<String, Student>();
	
	public StudentMap() {
		createData();
	}
	
	/**
	check if the student exists   
	@ parameter: id, student ID
	@ return the corresbonding Student object
	@ example: StudentMap.hasStudent(985002016), 
		then we get the object of the student.
	@ Time estimate: O(n)
	*/
	
	public boolean hasStudent(String id) {
		return map.containsKey(id);
	}
	
	/**
	add a new student with his id to the dictionary  
	@ parameter: id, student ID; student, information about the students
	@ nothing to return 
	@ example: StudentMap.setStudent(985002016, Student 王炳淳's object), 
		then we secessfully involve a new student.
	@ Time estimate: O(1)
	*/
	
	public void setStudent(String id, Student student) {
		if (id != null && student != null) {
			map.put(id, student);
		}
	}
	
	/**
	provide the access to a student if needed.  
	@ parameter: id, studentID
	@ return the student's information, in the type of object
	@ example: StudentMap.getStudent(985002016), then we get 王炳淳's profile
	@ Time estimate: O(1)
	*/
	
	public Student getStudent(String id) {
		return hasStudent(id) ? map.get(id) : null; 
	}
	
	/**
	Provide the access to every students' object and gather them into an array.
	@ no parameters needed
	@ return an array of all the students' object 
	@ example: StudentMap.allValues(), 
		then we get every Student's object.
	@ Time estimate: O(n)
	*/
	
	public Student[] allValues() {
		return map.values().toArray(new Student[0]);
	}
	
	/**
	New a student object and set its attributes as the given data.  
	@ parameter: an array of data, which records the student's name, ID, and scores. 
	@ return a new student object with all attribute set. 
	@ example: StudentMap.createStudent([985002009,呂哲光,81,83,86,82,90]), 
		then we get a Student object with all value assigned.
	@ Time estimate: O(n)
	*/
	
	private static Student createStudent(String[] data) {
		if (data.length != 7) {
			return null;
		}
		String id = data[0];
		String name = data[1];
		String[] scores = Arrays.copyOfRange(data, 2, 7);
		double[] originalScores= Arrays.stream(scores).mapToDouble(Double::parseDouble).toArray();
		Student student = new Student(id, name);
		student.setOriginalScores(originalScores);
		student.setWeightedScores(originalScores);
		return student;
	}
	
	/**
	Read students' information from an outside file, then call createStudent() to set the values of the attributes.  
	@ no parameters needed 
	@ nothing to return  
	@ example: StudentMap.createData(), 
		then we get a Student object with all value assigned.
	@ Time estimate: O(n)
	*/
	
	private void createData() {
		try {
			File file = new File("src\\input.txt");
			Scanner reader = new Scanner(file, "UTF-8");
			while (reader.hasNextLine()) {
				String[] data = reader.nextLine().trim().split("\\s+");
				Student student = createStudent(data);
				setStudent(student.getId(), student);
			}
			reader.close();
		} catch (FileNotFoundException e) {
			System.out.println("No user data");
			e.printStackTrace();
		}
	}
	
	/**
	sort the students order with their average score
	@ no parameters needed 
	@ return a rank-sorted array of the student object
	@ example: StudentMap.allValuesByRankAsec(), 
		then we get a rank-sorted array of the student object
	@ Time estimate: O(n^2)
	*/
	
	public Student[] allValuesByRankAsec() {
		TreeMap<String, Student> sortedByRank = new TreeMap<String, Student>(new RankCmp(map));
		sortedByRank.putAll(map);
		return sortedByRank.values().toArray(new Student[0]);
	}
}
