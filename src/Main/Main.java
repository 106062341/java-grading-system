package Main;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
	private static Student user = null;
	private static StudentMap studentMap = new StudentMap();
	private static double[] weights = { 0.1, 0.1, 0.1, 0.3, 0.4 };
	private static Scanner scanner = null;
	public final static String scoresPrefix = "lab 1\tlab 2\tlab 3\tmidterm\tfinal-exam\n==================================================";
	
	public static StudentMap getStudentMap() { return studentMap; }
	
	/**
	Read the input value from keyboard.  
	@ no parameters needed 
	@ return the string we typed from the keyboard 
	@ example: readStringInput(), 
		then we can type values from our keyboards then we'll get the same string.
	@ Time estimate: O(1)
	*/
	
	private static String readStringInput() {
		try {
			scanner = new Scanner(System.in);
			String str = scanner.nextLine();
			return str;
		} catch (Exception e) {
			scanner.close();
			return "";
		}
	}
	
	/**
	Show messages on the screen.  
	@ messages, any message we want it to show.
	@ nothing to return  
	@ example: response('hi'), 
		then our screen shows the value 'hi'.
	@ Time estimate: O(1)
	*/
	
	private static void response(String ...messages) {
		for (String message : messages) {
			System.out.println(message);
		}
	}
	
	/**
	choose to exit or to set the variable 'user' as the student object if a certain student ID exists.   
	@ no parameters needed 
	@ return true if the input student ID exists, false otherwise.  
	@ example: setUser(985002009), 
		return true.
			setUser(E), return false.	
	@ Time estimate: O(n)
	*/
	
	public static boolean setUser() {
		response("Login...", "Please enter your student ID (or type 'E' to exit):");
		String id = readStringInput();
		if (id.length() == 1 && id.charAt(0) == 'E') {
			return parseCommand(id);
		}
		if (!studentMap.hasStudent(id)) {
			response("No such user");
			return false;
		}
		user = studentMap.getStudent(id);
		response("Welcome, user " + user.getName() + " :)");
		return true;
	}
	
	/**
	replace all spaces in the string with none. 
	@ parameter:str, the string to be modified 
	@ return the string we replace all spaces with none. 
	@ example: purifyString('E '), 
		then we get 'E' .
	@ Time estimate: O(string.length)
	*/
	
	private static String purifyString(String str) {
		return str.replaceAll("\\s*", "");
	}
	
	/**
	Show manual on the screen.  
	@ no parameters needed
	@ nothing to return  
	@ example:showManual(), 
		then our screen shows ''.
	@ Time estimate: O(1)
	*/
	
	public static void showManual() {
		response(
				"=== Manual ===",
				"1) H - 顯示指令集",
				"2) G - 顯示成績",
				"3) R - 顯示排名",
				"4) A - 顯示平均",
				"5) W - 更新配分",
				"6) E - 離開系統\n"
			);
	}
	
	/**
	Show the user's score on the screen.  
	@ no parameters needed
	@ nothing to return  
	@ example:showUserScore(), 
		then our screen shows 'lab 1	lab 2	lab 3	midterm	final-exam
							==================================================
								85	84 	92	92	80'.
	@ Time estimate: O(1)
	*/
	
	public static void showUserScore() {
		double[] scores = user.getOriginalScores();
		if (scores != null) {
			String results = "";
			for (double score: scores) {
				results = results + String.valueOf(score) + "\t";
			}
			response(
					scoresPrefix,
					results + "\n"
				);
		}
	}
	
	/**
	to check if the input weight is legal
	@ para:v, which stands for weight
	@ return true if the input weight is legal, false otherwise.
	@ example: isValidWeight(0.2), return true.
	@ Time estimate: O(1)
	 */
	
	private static boolean isValidWeight(double v) {
		return v >= 0.000 && v <= 1.000;
	}
	
	/**
	to check if the input weights and the amount of weights are legal
	@ para:strs, an array of the input weights
	@ return true if the input weights and the amount of the input weights are legal, false otherwise.
	@ example: isValidInput([0.2,0.2,0.2,0.2,0.2]), return true.
	@ Time estimate: O(1)
	*/
	
	private static boolean isValidInput(String[] strs) {
		try {
			if (strs.length != weights.length) {
				response("Invalid values(Expected " + weights.length + " values, get " + strs.length + ")");
				return false;
			}
			if(!Arrays.stream(strs).mapToDouble(Double::valueOf).allMatch(x -> isValidWeight(x))) {
				return false;
			}
			double sum = Arrays.stream(strs).mapToDouble(Double::valueOf).sum();
			if(sum != 1.0) {
				response("Expect sum of weights should be 1, got " + sum);
				return false;
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	Get every students' scores then times the weight.
	@ no parameter needed
	@ nothing to return  
	@ example: updateStudentsWeightScore(), 
		then every scores multiplied by the weight.
	@ Time estimate: O(n)
	*/
	
	private static void updateStudentsWeightedScore() {
		Student[] studentList = studentMap.allValues();
		for (int i = 0; i < studentList.length; i++) {
			Student student = studentList[i];
			double [] originalScores = student.getOriginalScores();
			double[] weightedScores = Arrays.copyOf(originalScores, originalScores.length);
			for (int j = 0; j < weightedScores.length; j++) {
				weightedScores[j] = (double) (originalScores[j] * weights[j]);
			}
			student.setWeightedScores(weightedScores);
		}
	}
	
	/**
	type 5 values then assign them to the weight value  
	@ no parameters needed
	@ return true if sucessfully update the weight 
	@ example: setWeights(), 
		then can assign the value of the weights by typing 5 values.
	@ Time estimate: O(1)
	*/
	
	private static boolean setWeights() {
		response("Please enter 5 values to continue(in order of lab 1, lab 2, lab 3, midterm, final exam)");
		String[] strs = readStringInput().trim().split("\\s+");
		if (!isValidInput(strs)) {
			response("There is invalid value in input, set weights failed");
			return false;
		}
		for (int i = 0; i < weights.length; i++) {
			weights[i] = Double.valueOf(strs[i]);
		}
		response("Set completed",
				"lab 1 = " + weights[0] + ", lab 2 = " + weights[1] + ", lab 3 = " + weights[2] + ", midterm = " + weights[3] + ", final exam = " + weights[4]
			);
		return true;
	}
	
	/**
	Show the student's average score on the screen.  
	@ no parameters needed
	@ nothing to return  
	@ example: showUserAverage(), 
		then our screen present the student's average score.
	@ Time estimate: O(1)
	*/
	
	public static void showUserAverage() {
		double score = user.getWeightedAverageScore();
		response(String.format("%.1f", score));
	}
	
	/**
	sort the students' order according to their average score  
	@ no parameters needed
	@ nothing to return  
	@ example: updateStudentsRank(), 
		then the students' score rank is updated.
	@ Time estimate: O(n^2)
	*/
	
	public static void updateStudentsRank() {
		Student[] students = studentMap.allValuesByRankAsec();
		for (int i = 0; i < students.length; i++) {
			students[i].setRank(i + 1);
		}
	}
	
	/**
	multiply every student's scores by weights then sort them.  
	@ no parameters needed
	@ nothing to return  
	@ example: updateWeights(), 
		then the students' scores and rank are updated.
	@ Time estimate: O(n^2)
	*/
	
	public static void updateWeights() {
		if(setWeights()) {
			updateStudentsWeightedScore();
			updateStudentsRank();
		}
	}
	
	/**
	Show the student's rank on the screen.  
	@ no parameters needed
	@ nothing to return  
	@ example: showUserRank(), 
		then our screen present the student's rank.
	@ Time estimate: O(1)
	*/
	
	public static void showUserRank() {
		int rank = user.getRank();
		response(String.valueOf(rank));
	}
	
	/**
	stop the program  
	@ no parameters needed
	@ nothing to return  
	@ example: exit(), 
		then the system exit.
	@ Time estimate: O(1)
	*/
	
	public static void exit() {
		if (user != null) {
			response("Logout");
			user = null;
			return;
		}
		response("Exit grading system");
		System.exit(0);
	}
	
	/**
	telling invalid instructions  
	@ no parameters needed
	@ nothing to return  
	@ example: handleInvalidInstruction(), 
		then our screen shows 'Invalid instruction'.
	@ Time estimate: O(1)
	*/
	
	private static void handleInvalidInstruction() {
		response("Invalid instruction");
	}
	
	/**
	a user interface  
	@ parameter: command, the instruction command
	@ return true if sucessfully end the code, false if exit.
	@ example: parseCommand('H'), 
		then show the Manual.
	@ Time estimate: O(1)
	*/
	
	private static boolean parseCommand(String command) {
		command = purifyString(command);
		switch (command.charAt(0)) {
			case 'H': showManual(); break;
			case 'G': showUserScore();				break;
			case 'W': updateWeights();				break;
			case 'A': showUserAverage();			break;
			case 'R': showUserRank();				break;
			case 'E': exit(); 						return false;
			default: handleInvalidInstruction();	break;	
		}
		return true;
	}
	
	/**
	loop the command part  
	@ no parameters needed
	@ nothing to return 
	@ example: launch(), 
		then the code start running until it exit.
	@ Time estimate: O(infinity)
	*/
	
	private static void launch() {
		boolean parseSucess = false;
			String command = null;
			do {
				response("Enter next instruction:");
				command = readStringInput();
				parseSucess = parseCommand(command);
			} while(command != null && parseSucess);
	}
	
	
	public static void main(String[] args) {
		while(true) {
			while(!setUser());
			updateStudentsWeightedScore();
			updateStudentsRank();
			showManual();
			launch();
		}
	}
}
