package Main;

import java.lang.reflect.Array;
import java.util.stream.DoubleStream;
import java.lang.Math;

public class Student {
	private String id = null;
	private String name = null;
	private int DEFAULT_RANK = -1;
	private int rank = DEFAULT_RANK;
	private LabGrade labGrade = new LabGrade();
	private ExamGrade examGrade = new ExamGrade();
	private LabGrade weightedLabGrade = new LabGrade();
	private ExamGrade weightedExamGrade = new ExamGrade();
	
	public Student(String ...args) {
		if (args.length == 2) {
			id = args[0];
			name = args[1];
		}
	}
	
	/**
	assign the input value to students' id 
	@ para:id, which stands for student id 
	@ nothing to return 
	@ example: Student.setId(975002501), then a student's id will be set as 975002501.
	@ Time estimate: O(1)
	*/
	
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	assign the input value to students' name 
	@ para:name, which stands for student name 
	@ nothing to return 
	@ example: Student.setName(張廷瑄), then a student's name will be set as 張廷瑄.
	@ Time estimate: O(1)
	*/
	
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	assign the input value to students' rank 
	@ para:rank, which stands for student rank 
	@ nothing to return 
	@ example: Student.setRank(1), then a student's rank will be set as 1.
	@ Time estimate: O(1)
	*/
	
	public void setRank(int rank) {
		this.rank = rank > 0 ? rank : DEFAULT_RANK;
	}
	
	/**
	assign the input array of values to students' scores if the input scores and the amount of scores are all valid.
	@ para:scores, an array, involving students' scores 
	@ nothing to return 
	@ example: Student.setOriginalScores([80,86,98,94,87]), then a student's scores will be set as the parameter.
	@ Time estimate: O(1)
	*/
	
	public void setOriginalScores(double [] scores) {
		if (scores.length != 5 || DoubleStream.of(scores).anyMatch(x -> !labGrade.isValid(x))) {
			System.out.println("Setting scores failed(Invalid score value)");
			return;
		}
		labGrade.setScore1(scores[0]);
		labGrade.setScore2(scores[1]);
		labGrade.setScore3(scores[2]);
		examGrade.setMidterm(scores[3]);
		examGrade.setFinalterm(scores[4]);
	}
	
	/**
	assign the input array of values to students' weighted scores if the input scores and the amount of scores are all valid.
	@ para:scores, an array, involving students' weighted scores 
	@ nothing to return 
	@ example: Student.setWeightedScores([80,86,98,94,87), then a student's scores will be set as the parameter.
	@ Time estimate: O(1)
	*/
	
	public void setWeightedScores(double [] scores) {
		if (scores.length != 5 || DoubleStream.of(scores).anyMatch(x -> !labGrade.isValid(x))) {
			System.out.println("Setting scores failed(Invalid score value)");
			return;
		}
		weightedLabGrade.setScore1(scores[0]);
		weightedLabGrade.setScore2(scores[1]);
		weightedLabGrade.setScore3(scores[2]);
		weightedExamGrade.setMidterm(scores[3]);
		weightedExamGrade.setFinalterm(scores[4]);
	}
	
	/**
	provide the access to student Id, names, or rank for other objects if needed.  
	@ no parameters
	@ return the student's ID/name/rank
	@ example: Student.getId(), then return id
	@ Time estimate: O(1)
	*/
	
	public String getId() { return id; }
	public String getName() { return name; }
	public int getRank() { return rank; }
	
	/**
	combine two double type arrays into one   
	@ array a, array b 
	@ return the a, b combined array
	@ example: Student.concat(labGrade.getScores(),examGrade.getScores()), 
		then we get a merge of the labgrades and examgrades.
	@ Time estimate: O(1)
	*/
	
	private static double[] concat(double[] a, double[] b) {
		int aLen = a.length;
		int bLen = b.length;
		
		double c[] = (double[]) Array.newInstance(a.getClass().getComponentType(), aLen + bLen);
		System.arraycopy(a, 0, c, 0, aLen);
		System.arraycopy(b, 0, c, aLen, bLen);
		return c;
	}
	
	/**
	to check if the input array of score is legal
	@ para:an array of a students' scores 
	@ return true if the all the score in the array is legal, false otherwise.
	@ example: Student.isValidScore(50), return true.
	@ Time estimate: O(1)
	 */
	
	private boolean isValidScore(double [] scores) {
		return DoubleStream.of(scores).allMatch(score -> score >= 0);
	}
	
	/**
	provide the access to a student's score, weighted scores, or average scores for other objects if needed.  
	@ no parameters
	@ return the student's score, weighted scores, or average scores
	@ example: Student.getOriginalScores(), then return unweighted scores
	@ Time estimate: O(1)
	*/
	
	public double[] getOriginalScores() {
		double[] scores = concat(
				labGrade.getScores(),
				examGrade.getScores()
			);
		return isValidScore(scores) ? scores : null;
	}
	
	public double[] getWeightedScores() {
		double[] scores = concat(
				weightedLabGrade.getScores(),
				weightedExamGrade.getScores()
			);
		return isValidScore(scores) ? scores : null;
	}
	
	public double getWeightedAverageScore() {
		double[] scores = getWeightedScores();
		if (scores == null) {
			return -1.00;
		}
		double average = (double) DoubleStream.of(scores).sum();
		return average;
	}
}
