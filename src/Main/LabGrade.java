package Main;

public class LabGrade {
	private int LOWER_BOUND = 0;
	private int DEFAULT_SCORE = -1;
	private double score1 = DEFAULT_SCORE;
	private double score2 = DEFAULT_SCORE;
	private double score3 = DEFAULT_SCORE;
	
	/**
	to check if the input score is legal
	@ para:v, which stands for score; if score>=LOWER_BOUND, we consider it legal then resume running the code.
	@ return true if the input score is legal, false otherwise.
	@ example: LabGrade.isValid(50), return true.
	@ Time estimate: O(1)
	*/
	
	public boolean isValid(double v) {
		return v >= LOWER_BOUND;
	}
	
	/**
	set student's lab scores by calling setScore functions.
	@ para:scores, an array of input scores
	@ nothing to return 
	@ example: new LabGrade([50,60,70]), then a student's lab score will be set as 50,60,70.
	@ Time estimate: O(1)
	*/
	
	public LabGrade(double ...scores) {
		if (scores.length == 3) {
			setScore1(scores[0]);
			setScore2(scores[1]);
			setScore3(scores[2]);
		}
	}

	/**
	assign the input scores to students' scores if isValid(v) returns true.
	@ para:v, which stands for score
	@ nothing to return 
	@ example: LabGrade.setScore1(50), then a student's lab score1 will be set as 50.
	@ Time estimate: O(1)
	*/
	
	public void setScore1(double v) {
		this.score1 = isValid(v) ? v : DEFAULT_SCORE;
	}
	public void setScore2(double v) {
		this.score2 = isValid(v) ? v : DEFAULT_SCORE;
	}
	public void setScore3(double v) {
		this.score3 = isValid(v) ? v : DEFAULT_SCORE;
	}
	
	/**
	provide the access to Lab scores for other objects if needed.  
	@ no parameters
	@ return the student's score
	@ example: LabGrade.getScore1(), then return score1
	@ Time estimate: O(1)
	*/
	
	public double getScore1() { return score1; }
	public double getScore2() { return score2; }
	public double getScore3() { return score3; }
	
	/**
	provide the access to the mix of the lab scores for other objects if needed.  
	@ no parameters
	@ return the student's lab scores
	@ example: LabGrade.getScores(), then we get an array including each lab score.
	@ Time estimate: O(1)
	*/

	public double[] getScores() {
		double[] scores = {score1, score2, score3};
		return scores;
	}
}
