package Main;

public class TerminateException extends SecurityException {
	private static final long serialVersionUID = 1L;
	public final int status;
	
	public TerminateException(int status) {
		super("Exit grading system");
		this.status = status;
	}
}
