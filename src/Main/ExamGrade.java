package Main;

public class ExamGrade {
	private int LOWER_BOUND = 0;
	private int DEFAULT_SCORE = -1;
	private double midterm = DEFAULT_SCORE;
	private double finalterm = DEFAULT_SCORE;
	
	/**
	to check if the input score is legal
	@ para:v, which stands for score; if score>=LOWER_BOUND, we consider it legal then resume running the code.
	@ return true if the input score is legal, false otherwise.
	@ example: ExamGrade.isValid(50), return true.
	@ Time estimate: O(1)
	*/
	
	public boolean isValid(double v) {
		return v >= LOWER_BOUND;
	}
	
	public ExamGrade(double ...scores) {
		if (scores.length == 2) {
			setMidterm(scores[0]);
			setFinalterm(scores[1]);
		}
	}
	
	/**
	assign the input scores to students' scores if isValid(v) returns true.
	@ para:v, which stands for score
	@ nothing to return 
	@ example: ExamGrade.setMidterm(50), then a student's midterm score will be set as 50.
	@ Time estimate: O(1)
	*/
	
	public void setMidterm(double v) {
		this.midterm = isValid(v) ? v : DEFAULT_SCORE;
	}
	public void setFinalterm(double v) {
		this.finalterm = isValid(v) ? v : DEFAULT_SCORE;
	}

	/**
	provide the access to midterm/finalterm scores for other objects if needed.  
	@ no parameters
	@ return the student's midterm score
	@ example: ExamGrade.getMidterm(), then return midterm
	@ Time estimate: O(1)
	*/
	
	public double getMidterm() { return midterm; }
	public double getFinalterm() { return finalterm; }
	
	/**
	provide the access to the mix of the midterm/finalterm scores for other objects if needed.  
	@ no parameters
	@ return an array of the student's midterm and final scores
	@ example: ExamGrade.getScores(), then we get an array including midterm/finalterm scores.
	@ Time estimate: O(1)
	*/
	
	public double[] getScores() {
		double[] scores = { midterm, finalterm };
		return scores;
	}
}
