package test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import Main.ExamGrade;

class examGradeTest {
	private int underBound = -1;
	private int inBound = 100;
	private int upBound = 101;
	private int defaultScore = -1;

	@BeforeEach
	void init() throws Exception {
		System.out.println("Start exam grade test...");
	}
	
	@AfterEach
	void tearDown() throws Exception {
		System.out.println("Finish exam grade test...");
	}
	
	/**
	*Test function: ExamGrade.examGrade
	*test data: upBound:101  
	*/
	
	@Test
	void testInvalidInit() {
		ExamGrade examGrade = new ExamGrade(upBound);
		assertEquals(defaultScore, examGrade.getMidterm());
		assertEquals(defaultScore, examGrade.getFinalterm());
	}
	
	/**
	*Test function: ExamGrade.getMidterm, ExamGrade.getFinalterm, ExamGrade.getScores
	*test data: midterm score: underBound  final-term score: inBound
	*/
	
	@Test
	void testGetScore() {
		ExamGrade examGrade = new ExamGrade(underBound, inBound);
		
		assertEquals(defaultScore, examGrade.getMidterm());
		assertEquals(inBound, examGrade.getFinalterm());

		double[] arr = { defaultScore, inBound };
		assertArrayEquals(arr, examGrade.getScores());
		
		examGrade = new ExamGrade(underBound, inBound, upBound);
		assertEquals(defaultScore, examGrade.getMidterm());
		assertEquals(defaultScore, examGrade.getFinalterm());
		
		examGrade = new ExamGrade(upBound);
		assertEquals(defaultScore, examGrade.getMidterm());
		assertEquals(defaultScore, examGrade.getFinalterm());
	}
	
	/**
	*Test function: ExamGrade.setMidterm, ExamGrade.setFinalterm
	*test data: underBound:-1  inBound: 100
	*/
	
	@Test
	void testSetScore() {
		ExamGrade examGrade = new ExamGrade(underBound, inBound);
		
		examGrade.setMidterm(inBound);
		assertEquals(inBound, examGrade.getMidterm());
		examGrade.setFinalterm(underBound);
		assertEquals(defaultScore, examGrade.getFinalterm());
		
		examGrade.setMidterm(upBound);
		assertEquals(upBound, examGrade.getMidterm());
		examGrade.setFinalterm(inBound);
		assertEquals(inBound, examGrade.getFinalterm());

		double[] arr = { upBound, inBound };
		assertArrayEquals(arr, examGrade.getScores());
	}
}
