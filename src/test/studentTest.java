package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import Main.Student;

class studentTest {
	private int DEFAULT_RANK = -1;
	private double DEFAULT_AVERAGE = -1.00;
	private String id = "106062339";
	private String name = "簡維成";
	private int invalid_rank = DEFAULT_RANK;
	private int valid_rank = 56;
	private double[] valid_scores = { 12, 23, 34, 45, 56 };
	private double[] invalid_scores = { -12, 23, 340, 45, 56 };
	private double[] lab_scores = { 12, 23, 34 };
	
	@BeforeEach
	void init() throws Exception {
		System.out.println("Start student test...");
	}
	
	@AfterEach
	void tearDown() throws Exception {
		System.out.println("Finish student test...");
	}

	/**
	*Test function: Student.Student
	*test data: id:'106062339', name: '²����'  
	*/
	
	@Test
	void testValidInit() {
		Student student = new Student(id, name);
		
		assertEquals(id, student.getId());
		assertEquals(name, student.getName());
	}
	
	/**
	*Test function: Student.setId, Student.setName, Student.setOriginalScores, Student.setRank, Student.setWeightedScores
	*test data: id:'106062339', name: '²����', valid_rank : 56, valid_scores : { 12, 23, 34, 45, 56 }, 
				invalid_scores : { -12, 23, 340, 45, 56 }, lab_scores : { 12, 23, 34 }
	*/
	
	@Test
	void testValidSetStudent() {
		Student student = new Student();
		
		assertNull(student.getId());
		assertNull(student.getName());
		assertEquals(DEFAULT_RANK, student.getRank());
		assertNull(student.getWeightedScores());
		assertNull(student.getOriginalScores());
		assertEquals(DEFAULT_AVERAGE, student.getWeightedAverageScore());
		
		student.setId(id);
		student.setName(name);
		student.setRank(valid_rank);
		student.setOriginalScores(valid_scores);
		student.setWeightedScores(valid_scores);
		
		assertEquals(id, student.getId());
		assertEquals(name, student.getName());
		assertEquals(valid_rank, student.getRank());
		assertArrayEquals(valid_scores, student.getWeightedScores());
		assertArrayEquals(valid_scores, student.getOriginalScores());
		assertEquals(170.00, student.getWeightedAverageScore());
	}

	/**
	*Test function: Student.setId, Student.setName, Student.setOriginalScores, Student.setRank, Student.setWeightedScores
	*test data: id:'106062339', name: '²����', valid_scores : { 12, 23, 34, 45, 56 }, 
				invalid_scores : { -12, 23, 340, 45, 56 }, lab_scores : { 12, 23, 34 }, invalid_rank : -1
	*/
	
	@Test
	void testInvalidSetStudentCase1() {
		Student student = new Student();
		student.setOriginalScores(valid_scores);
		student.setWeightedScores(valid_scores);
		assertArrayEquals(valid_scores, student.getWeightedScores());
		assertArrayEquals(valid_scores, student.getOriginalScores());

		student.setId(id);
		student.setName(name);
		student.setRank(invalid_rank);
		student.setOriginalScores(invalid_scores);
		student.setWeightedScores(invalid_scores);
		
		assertEquals(id, student.getId());
		assertEquals(name, student.getName());
		assertEquals(DEFAULT_RANK, student.getRank());
		assertArrayEquals(valid_scores, student.getWeightedScores());
		assertArrayEquals(valid_scores, student.getOriginalScores());
		assertEquals(170.00, student.getWeightedAverageScore());

		student.setId(name);
		student.setName(id);
		assertEquals(name, student.getId());
		assertEquals(id, student.getName());
	}

	/**
	*Test function: Student.setOriginalScores, Student.setWeightedScores
	*test data:  valid_scores : { 12, 23, 34, 45, 56 }, lab_scores : { 12, 23, 34 }
	*/
	
	@Test
	void testInvalidSetStudentCase2() {
		Student student = new Student();
		student.setOriginalScores(valid_scores);
		student.setWeightedScores(valid_scores);
		assertArrayEquals(valid_scores, student.getWeightedScores());
		assertArrayEquals(valid_scores, student.getOriginalScores());
		
		student.setOriginalScores(lab_scores);
		student.setWeightedScores(lab_scores);

		assertArrayEquals(valid_scores, student.getWeightedScores());
		assertArrayEquals(valid_scores, student.getOriginalScores());
	}
}
