package test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import Main.Main;
import Main.NoExitSecurityManager;
import Main.Student;
import Main.TerminateException;

import java.io.PrintStream;
import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;

class integrationTest {
	private final ByteArrayOutputStream byteArrayOutContent = new ByteArrayOutputStream();;
	private final PrintStream originalOutContent = System.out; 
	private final InputStream originalInContent = System.in;
	private final int exitStatusCode = 0;
	private final String originalWeights = "0.1 0.1 0.1 0.3 0.4";
	private final String validWeights = "0.4 0.2 0.1 0.2 0.1";
	
	private Student validStudent = new Student("962001051", "李威廷");
	private final String validOriginalRank = "63";
	private final String validOriginalScores = "81.0\t32.0\t50.0\t90.0\t93.0";
	private final String validOriginalAverage = "80.5"; // 81*0.1+32*0.1+5+90*0.3+93*0.4
	private final String validOriginalScoresSum = "346.0";
	private final String validAdjustiedAverage = "71.1"; // 81*0.4+32*0.2+5+90*0.2+93*0.1
	private final String validAdjustiedRank = "63";
	
	private Student validStudentCase2 = new Student("985002002", "林芯妤");
	private final String validOriginalScoresCase2 = "99.0\t81.0\t91.0\t92.0\t95.0";
	private final String validAdjustiedAverageCase2 = "92.8"; // 99*0.4+81*0.2+91*0.1+92*0.2+95*0.1
	private final String validAdjustiedRankCase2 = "11";
	
	private void assertEqualOutput(String str) {
		try {
			assertEquals(str.trim(), byteArrayOutContent.toString().trim());
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	private void setSystemInput(String str) {	
		try {
			System.setIn(new ByteArrayInputStream(str.getBytes()));
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	private void clearOutputStream() {
		byteArrayOutContent.reset();
	}
	
	/**
	 * test function: Main.setUser()
	 * test data: specified id
	 */
	private void assertValidLogin(String id) {
		setSystemInput(id);
		assertTrue(Main.setUser());
		clearOutputStream();
	}
	
	/**
	 * test function: Main.exit()
	 * test data: output string "logout"(whether matches function result)
	 */	
	private void assertValidLogout() {
		clearOutputStream();
		Main.exit();
		assertEqualOutput("Logout");
	}
	
	@BeforeEach
	void init() throws Exception {
		clearOutputStream();
		System.out.println("Start student test...");
		System.setOut(new PrintStream(byteArrayOutContent));
	}
	
	@AfterEach
	void tearDown() throws Exception {
		System.setOut(originalOutContent);
		System.out.println("Finish student test...");
		System.setIn(originalInContent);
	}
	
	/**
	 * test functions:
	 * 		Main.setUser()
	 * 		Main.showUserScore()
	 * 		Main.showUserAverage()
	 * 		Main.updateStudentsRank()
	 * 		Main.showUserRank()
	 * 		Main.updateWeights()
	 * 		Main.exit()
	 * 		Main.showManual()
	 * 	test data
	 * 		validStudent = new Student("962001051", "李威廷")
	 * 		validOriginalScores = 81.0 32.0 50.0 90.0 93.0
	 * 		validOriginalScoresSum = 346
	 * 		validOriginalRank = 63
	 * 		validWeights = 0.4 0.2 0.1 0.2 0.1
	 * 		validAdjustiedAverage = 71.1
	 * 		validAdjustiedRank = 63
	 * 		validStudentCase2 = new Student("985002002", "林芯妤")
	 * 		validOriginalScoresCase2 = 99.0 81.0 91.0 92.0 95.0
	 * 		validAdjustiedAverageCase2 = 92.8
	 * 		validAdjustiedRankCase2 = 11
	 * 		exitStatusCode = 0
	 * 		manual output = "=== Manual ===\r\n1) H - 顯示指令集\r\n2) G - 顯示成績\r\n3) R - 顯示排名\r\n4) A - 顯示平均\r\n5) W - 更新配分\r\n6) E - 離開系統"
	 */
	@Test
	void testNormalScenairo() {		
		assertValidLogin(validStudent.getId());

		Main.showUserScore();
		assertEqualOutput(Main.scoresPrefix + "\r\n" + validOriginalScores);
		clearOutputStream();
		
		Main.showUserAverage();
		assertEqualOutput(validOriginalScoresSum);
		clearOutputStream();
		
		Main.updateStudentsRank();
		
		Main.showUserRank();
		assertEqualOutput(validOriginalRank);
		
		setSystemInput(validWeights);
		Main.updateWeights();
		clearOutputStream();

		Main.showManual();
		assertEqualOutput("=== Manual ===\r\n1) H - 顯示指令集\r\n2) G - 顯示成績\r\n3) R - 顯示排名\r\n4) A - 顯示平均\r\n5) W - 更新配分\r\n6) E - 離開系統");
		clearOutputStream();
		
		Main.showUserScore();
		assertEqualOutput(Main.scoresPrefix + "\r\n" + validOriginalScores);
		clearOutputStream();
		
		Main.showUserAverage();
		assertEqualOutput(String.valueOf(validAdjustiedAverage));
		clearOutputStream();
		
		Main.showUserRank();
		assertEqualOutput(validAdjustiedRank);
		
		assertValidLogout();
		assertValidLogin(validStudentCase2.getId());	

		Main.showUserScore();
		assertEqualOutput(Main.scoresPrefix + "\r\n" + validOriginalScoresCase2);
		clearOutputStream();
		
		Main.showUserAverage();
		assertEqualOutput(validAdjustiedAverageCase2);
		clearOutputStream();

		Main.showUserRank();
		assertEqualOutput(validAdjustiedRankCase2);
		
		assertValidLogout();
		System.setSecurityManager(new NoExitSecurityManager());
		try {
			Main.exit();
		} catch (TerminateException e) {
			assertEquals(exitStatusCode, e.status);
		}
		System.setSecurityManager(null);
		
	}
	
	/**
	 * test functions:
	 * 		Main.setUser()
	 * 		Main.showUserScore()
	 * 		Main.showUserAverage()
	 * 		Main.updateStudentsRank()
	 * 		Main.showUserRank()
	 * 		Main.updateWeights()
	 * 		Main.exit()
	 * 		Main.showManual()
	 * 	test data:
	 * 		originalWeights = 0.1 0.1 0.1 0.3 0.4
	 * 		validStudent = new Student("962001051", "李威廷")
	 * 		validOriginalScores = 81.0 32.0 50.0 90.0 93.0
	 * 		validOriginalAverage = "80.5"
	 * 		validWeights = 0.4 0.2 0.1 0.2 0.1
	 *		invalidInstruction = K;
	 *		invalidInstructionCase2 = \n;
	 *		invalidId = H;
	 *		invalidValueWeights = 0.1 . 0.3 0.3 0.2;
	 *		invalidLengthWeights = 0.2 0.2 0.2 0.4;
	 *		invalidSumhWeights = 0.2 0.1 0.1 0.4 0.1;
	 * 		manual output = "=== Manual ===\r\n1) H - 顯示指令集\r\n2) G - 顯示成績\r\n3) R - 顯示排名\r\n4) A - 顯示平均\r\n5) W - 更新配分\r\n6) E - 離開系統"
	 */
	@Test
	void testInappropriateScenairo() {
		final String invalidInstruction = "K";
		final String invalidInstructionCase2 = "\n";
		final String invalidId = "H";
		final String invalidValueWeights = "0.1 . 0.3 0.3 0.2";
		final String invalidLengthWeights = "0.2 0.2 0.2 0.4";
		final String invalidSumhWeights = "0.2 0.1 0.1 0.4 0.1";

		setSystemInput(invalidId);
		assertFalse(Main.setUser());
		clearOutputStream();
		
		setSystemInput(invalidInstruction);
		assertFalse(Main.setUser());
		clearOutputStream();
		
		setSystemInput(invalidInstructionCase2);
		assertFalse(Main.setUser());
		clearOutputStream();
		
		assertValidLogin(validStudent.getId());

		setSystemInput(originalWeights);
		Main.updateWeights();
		clearOutputStream();
		
		setSystemInput(invalidLengthWeights);
		Main.updateWeights();
		clearOutputStream();

		Main.showManual();
		assertEqualOutput("=== Manual ===\r\n1) H - 顯示指令集\r\n2) G - 顯示成績\r\n3) R - 顯示排名\r\n4) A - 顯示平均\r\n5) W - 更新配分\r\n6) E - 離開系統");
		clearOutputStream();
		
		Main.showUserAverage();
		assertEqualOutput(String.valueOf(validOriginalAverage));
		clearOutputStream();
		
		setSystemInput(invalidValueWeights);
		Main.updateWeights();
		clearOutputStream();
		
		Main.showUserAverage();
		assertEqualOutput(String.valueOf(validOriginalAverage));
		clearOutputStream();

		Main.showUserScore();
		assertEqualOutput(Main.scoresPrefix + "\r\n" + validOriginalScores);
		clearOutputStream();
		
		setSystemInput(invalidSumhWeights);
		Main.updateWeights();
		clearOutputStream();
		
		Main.showUserAverage();
		assertEqualOutput(String.valueOf(validOriginalAverage));
		clearOutputStream();
		
		setSystemInput(validWeights);
		Main.updateWeights();
		clearOutputStream();
		
		Main.showUserAverage();
		assertEqualOutput(String.valueOf(validAdjustiedAverage));
		clearOutputStream();
		
		Main.showUserRank();
		assertEqualOutput(validAdjustiedRank);
		clearOutputStream();
		
		Main.updateStudentsRank();

		Main.showUserRank();
		assertEqualOutput(validAdjustiedRank);
		clearOutputStream();
		
		assertValidLogout();
		setSystemInput(invalidId);
		assertFalse(Main.setUser());
		clearOutputStream();
		
		setSystemInput(invalidInstruction);
		assertFalse(Main.setUser());
		clearOutputStream();
		
		setSystemInput(invalidInstructionCase2);
		assertFalse(Main.setUser());
		clearOutputStream();
		
		assertValidLogin(validStudentCase2.getId());
	}

}
