package test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import Main.LabGrade;

class labGradeTest {
	private int underBound = -1;
	private int inBound = 100;
	private int upBound = 101;
	private int defaultScore = -1;
	
	@BeforeEach
	void init() throws Exception {
		System.out.println("Start lab grade test...");
	}
	
	@AfterEach
	void tearDown() throws Exception {
		System.out.println("Finish lab grade test...");
	}
	
	/**
	*Test function: LabGrade.LabGrade()
	*test data: inBound:100   
	*/
	
	@Test
	void testInvalidInit() {		
		LabGrade labGrade = new LabGrade(inBound, inBound, inBound, inBound);
		
		assertEquals(defaultScore, labGrade.getScore1());
		assertEquals(defaultScore, labGrade.getScore2());
		assertEquals(defaultScore, labGrade.getScore3());
	}
	
	/**
	*Test function: LabGrade.getScore1, LabGrade.getScore2, LabGrade.getScore3 LabGrade.getScores
	*test data: underBound:-1, inBound:100 upBound:101
	*/
	
	@Test
	void testGetScore() {
		LabGrade labGrade = new LabGrade(underBound, inBound, upBound);
		
		assertEquals(defaultScore, labGrade.getScore1());
		assertEquals(inBound, labGrade.getScore2());
		assertEquals(upBound, labGrade.getScore3());
		
		double[] arr = { defaultScore, inBound, upBound };
		assertArrayEquals(arr, labGrade.getScores());
	}
	
	/**
	*Test function: LabGrade.setScore1 LabGrade.setScore2 LabGrade.setScore3
	*test case: inBound:100 upBound:101 underBound:-1
	*/
	
	@Test
	void testSetScore() {
		LabGrade labGrade = new LabGrade();
		
		labGrade.setScore1(inBound);
		assertEquals(inBound, labGrade.getScore1());
		labGrade.setScore2(underBound);
		assertEquals(defaultScore, labGrade.getScore2());
		labGrade.setScore3(inBound);
		assertEquals(inBound, labGrade.getScore3());
		
		labGrade.setScore1(upBound);
		assertEquals(upBound, labGrade.getScore1());
		labGrade.setScore2(inBound);
		assertEquals(inBound, labGrade.getScore2());
		labGrade.setScore3(underBound);
		assertEquals(defaultScore, labGrade.getScore3());
	}
}
