package test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.PrintStream;
import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import Main.*;

class mainTest {
	private final ByteArrayOutputStream byteArrayOutContent = new ByteArrayOutputStream();;
	private final PrintStream originalOutContent = System.out; 
	private final InputStream originalInContent = System.in;
	private final StudentMap studentMap = Main.getStudentMap();
	private Student validStudent = new Student("985002003", "呂映萱");
	private Student validStudentCase2 = new Student("985002012", "史易秦");
	private String invalidId = "98500200";
	
	private void assertEqualOutput(String str) {
		try {
			assertEquals(str.trim(), byteArrayOutContent.toString().trim());
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	private void setSystemInput(String str) {	
		try {
			System.setIn(new ByteArrayInputStream(str.getBytes()));
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	private void clearOutputStream() {
		byteArrayOutContent.reset();
	}
	
	public void assertValidLogin(String id) {
		setSystemInput(id);
		assertTrue(Main.setUser());
		clearOutputStream();
	}
	
	void assertValidLogout() {
		clearOutputStream();
		Main.exit();
		assertEqualOutput("Logout");
	}
	
	@BeforeEach
	void init() throws Exception {
		clearOutputStream();
		System.out.println("Start main test...");
		System.setOut(new PrintStream(byteArrayOutContent));
	}
	
	@AfterEach
	void tearDown() throws Exception {
		System.setOut(originalOutContent);
		System.out.println("Finish main test...");
		System.setIn(originalInContent);
	}
	
	/**
	*Test function: setUser
	*test data: student.getOriginalScores()
	*/
	
	@Test
	void testLogin() {
		setSystemInput(validStudent.getId());
		assertTrue(Main.setUser());

		setSystemInput(invalidId);
		assertFalse(Main.setUser());

		setSystemInput(validStudentCase2.getId());
		assertTrue(Main.setUser());

	}

	/**
	*Test function: showManual
	*test data: the string of "=== Manual ===\r\n1) H - ��ܫ��O��\r\n2) G - ��ܦ��Z\r\n3) R - ��ܱƦW\r\n4) A - ��ܥ���\r\n5) W - ��s�t��\r\n6) E - ���}�t��"
	*/
	
	@Test
	void testShowManual() {
		Main.showManual();
		assertEqualOutput("=== Manual ===\r\n1) H - 顯示指令集\r\n2) G - 顯示成績\r\n3) R - 顯示排名\r\n4) A - 顯示平均\r\n5) W - 更新配分\r\n6) E - 離開系統");
		
		assertValidLogin(validStudent.getId());
		Main.showManual();
		assertEqualOutput("=== Manual ===\r\n1) H - 顯示指令集\r\n2) G - 顯示成績\r\n3) R - 顯示排名\r\n4) A - 顯示平均\r\n5) W - 更新配分\r\n6) E - 離開系統");
		assertValidLogout();
	}
	
	private String doubleArrayToStrings(double[] arr) {
		String str= "";
		for (double item: arr) {
			str = str + String.valueOf(item) + "\t";
		}
		return str + "\r\n";
	}
	
	/**
	*Test function: showUserScores
	*test data: student:{985002003 �f�M�� 83 89 94 97 80}
				student:{985002012 �v���� 94 80 84 86 86}
	*/
	
	
	@Test
	void testShowUserScores() {
		double[] scores = studentMap.getStudent(validStudent.getId()).getOriginalScores();
		String scoresStr = Main.scoresPrefix + "\r\n" + doubleArrayToStrings(scores);
		
		assertValidLogin(validStudent.getId());
		Main.showUserScore();
		assertEqualOutput(scoresStr);
		assertValidLogout();

		scores = studentMap.getStudent(validStudentCase2.getId()).getOriginalScores();
		scoresStr = Main.scoresPrefix + "\r\n" + doubleArrayToStrings(scores);
		
		assertValidLogin(validStudentCase2.getId());
		Main.showUserScore();
		assertEqualOutput(scoresStr);
		assertValidLogout();
	}
	
	/**
	*Test function: showUserAverage
	*test data: student:{985002003 �f�M�� 83 89 94 97 80}
				student:{985002012 �v���� 94 80 84 86 86}
	*/
	
	@Test
	void testShowAverage() {
		double sum = studentMap.getStudent(validStudent.getId()).getWeightedAverageScore();
		
		assertValidLogin(validStudent.getId());
		Main.showUserAverage();
		assertEqualOutput(String.valueOf(sum));

		sum = studentMap.getStudent(validStudentCase2.getId()).getWeightedAverageScore();
		assertValidLogin(validStudentCase2.getId());
		Main.showUserAverage();
		assertEqualOutput(String.valueOf(sum));
	}
	
	/**
	*Test function: updateWeights
	*test data: validWeights : "0.4 0.2 0.1 0.2 0.1", invalidValueWeights : "0.1 \n 0.3 0.3 0.2";
				invalidLengthWeights : "0.2 0.2 0.2 0.4", invalidSumhWeights : "0.2 0.1 0.1 0.4 0.1"
	*/
	
	@Test
	void testUpdateWeights() {
		final String validWeights = "0.4 0.2 0.1 0.2 0.1";
		final String invalidValueWeights = "0.1 \n 0.3 0.3 0.2";
		final String invalidLengthWeights = "0.2 0.2 0.2 0.4";
		final String invalidSumhWeights = "0.2 0.1 0.1 0.4 0.1";
		final double[] scores = studentMap.getStudent(validStudent.getId()).getOriginalScores();
		String scoresStr = Main.scoresPrefix + "\r\n";
		for (double score: scores) {
			scoresStr = scoresStr + String.valueOf(score) + "\t";
		}
		scoresStr += "\r\n";
		
		setSystemInput(invalidValueWeights);
		Main.updateWeights();
		setSystemInput(validWeights);
		Main.updateWeights();
		setSystemInput(invalidLengthWeights);
		Main.updateWeights();
		setSystemInput(validWeights);
		Main.updateWeights();
		setSystemInput(invalidSumhWeights);
		Main.updateWeights();
	}
	
	/**
	*Test function: StudentMap.getRank
	*test data: student:{985002003 �f�M�� 83 89 94 97 80}
				student:{985002012 �v���� 94 80 84 86 86}
	*/
	
	@Test
	void testGetRank() {		
		assertValidLogin(validStudent.getId());
		int rank = studentMap.getStudent(validStudent.getId()).getRank();
		Main.showUserRank();
		assertEqualOutput(String.valueOf(rank));
		clearOutputStream();
		
		Main.updateStudentsRank();
		
		rank = studentMap.getStudent(validStudent.getId()).getRank();
		Main.showUserRank();
		assertEqualOutput(String.valueOf(rank));
		clearOutputStream();

		assertValidLogin(validStudentCase2.getId());
		Main.updateStudentsRank();
		rank = studentMap.getStudent(validStudentCase2.getId()).getRank();
		Main.showUserRank();
		assertEqualOutput(String.valueOf(rank));
	}
	
	/**
	*Test function: Exit
	*test data: none
	*/
	
	@Test
	void testExitCase1() throws Exception {
		System.setSecurityManager(new NoExitSecurityManager());
		try {
			Main.exit();
		} catch (TerminateException e) {
			assertEquals(0, e.status);
		}
		System.setSecurityManager(null);
	}
	
	/**
	*Test function: Exit
	*test data: student:{985002003 �f�M�� 83 89 94 97 80}
	*/
	
	@Test
	void testExitCase2() throws Exception {
		System.setSecurityManager(new NoExitSecurityManager());
		
		assertValidLogin(validStudent.getId());
		
		Main.exit();
		assertEqualOutput("Logout");
		
		try {
			Main.exit();
		} catch (TerminateException e) {
			assertEquals(0, e.status);
		}
		System.setSecurityManager(null);
	}

}
