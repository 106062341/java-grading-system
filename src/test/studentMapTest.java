package test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Random;
import Main.StudentMap;
import Main.Student;

class studentMapTest {
	private String validId = "975002021";
	private String validName = "楊祺賢";
	private String invalidId = "97500202";
	private int TOTAL = 63;
	
	@BeforeEach
	void init() throws Exception {
		System.out.println("Start student map test...");
	}
	
	@AfterEach
	void tearDown() throws Exception {
		System.out.println("Finish student map test...");
	}
	
	/**
	*Test function: StudentMap.getStudent StudentMap.hasStudent, studentMap.allValues
	*test data: validId : "975002021", invalidId : "97500202"
	*/
	
	@Test
	void testGetStudent() {
		StudentMap studentMap = new StudentMap();
		
		assertTrue(studentMap.hasStudent(validId));
		assertEquals(validName, studentMap.getStudent(validId).getName());
		assertFalse(studentMap.hasStudent(invalidId));
		assertNull(studentMap.getStudent(invalidId));
		
		Student[] students = studentMap.allValues();
		assertEquals(TOTAL, students.length);
	}
	
	/**
	*Test function: StudentMap.setStudent
	*test data: validId : "975002021"
	*/
	
	@Test
	void testSetStudent() {
		StudentMap studentMap = new StudentMap();		
		Student[] students = studentMap.allValues();
		int randomIndex = new Random().nextInt(TOTAL);
		
		do {
			System.out.println("Create random student ...");
			Student randomStudent = students[randomIndex];
			studentMap.setStudent(validId, randomStudent);
			assertEquals(randomStudent.getId(), studentMap.getStudent(validId).getId());
			assertEquals(randomStudent.getName(), studentMap.getStudent(validId).getName());
			randomIndex = new Random().nextInt(TOTAL);
		} while (students[randomIndex].getId() == validId);
	}
	
	/**
	*Test function: StudentMap.allValuesByRankAsec
	*test data: student.getOriginalScores()
	*/
	
	@Test
	void testRankAsec() {
		double[] weights = { 0.4, 0.2, 0.1, 0.2, 0.1 };
		StudentMap studentMap = new StudentMap();
		Student[] students_default = studentMap.allValues();
		
		for (Student student: students_default) {
			double [] originalScores = student.getOriginalScores();
			double[] weightedScores = Arrays.copyOf(originalScores, originalScores.length);
			for (int j = 0; j < weightedScores.length; j++) {
				weightedScores[j] = (double) (originalScores[j] * weights[j]);
			}
			student.setWeightedScores(weightedScores);
		}

		Student[] students_by_rank = studentMap.allValuesByRankAsec();
		assertTrue(students_default.length == students_by_rank.length);
		for (int i = 0; i < TOTAL - 1; i++) {
			assertTrue(students_by_rank[i].getWeightedAverageScore() >= students_by_rank[i+1].getWeightedAverageScore());
		}
	}
}
